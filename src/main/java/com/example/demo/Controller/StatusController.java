package com.example.demo.Controller;

import com.example.demo.Entity.Shop;
import com.example.demo.Entity.Status;
import com.example.demo.Service.ShopService;
import com.example.demo.Service.StatusService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class StatusController {
    private final StatusService statusService;

    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }

    @RequestMapping(value="/status",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(statusService.getAll());

    }
    @RequestMapping(value="/status/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(statusService.getById(id));}
    @RequestMapping(value = "/status/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable    long id){
        statusService.delete(id);
    }
    @RequestMapping(value="/status",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Status shop){
        return ResponseEntity.ok(statusService.update(shop));
    }
    @RequestMapping(value="/status/name",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getNames() {
        return ResponseEntity.ok(statusService.getNames());

    }
}
