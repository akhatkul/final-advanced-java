package com.example.demo.Controller;

import com.example.demo.Entity.Factory;
import com.example.demo.Entity.Handlings;
import com.example.demo.Service.FactoryService;
import com.example.demo.Service.HandlingsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class HandlingsController {
    private final HandlingsService handlingsService;

    public HandlingsController(HandlingsService handlingsService) {
        this.handlingsService = handlingsService;
    }

    @RequestMapping(value="/handlings",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(handlingsService.getAll());

    }
    @RequestMapping(value="/handlings/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(handlingsService.getById(id));}
    @RequestMapping(value = "/handlings/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        handlingsService.delete(id);
    }
    @RequestMapping(value="/handlings",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Handlings handlings){
        return ResponseEntity.ok(handlingsService.update(handlings));
    }
}
