package com.example.demo.Service;

import com.example.demo.Entity.Delivery;
import com.example.demo.Entity.Factory;
import com.example.demo.Repository.DeliveryRepository;
import com.example.demo.Repository.FactoryRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class DeliveryService {
    private final DeliveryRepository deliveryRepository;

    public DeliveryService(DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(deliveryRepository.findAll());
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(deliveryRepository.findById(id));
    }

    public void delete(long id){
        deliveryRepository.deleteById(id);
    }
    public Delivery update(@RequestBody Delivery delivery){
        return  deliveryRepository.save(delivery);
    }

}
