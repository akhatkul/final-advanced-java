package com.example.demo.Repository;

import com.example.demo.Entity.OrderItem;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {
    @Transactional
    @Modifying
    @Query(value = "insert into order_item (order_id,product_id,quantity,price) values(:order_id,:product_id,:quantity,:price)", nativeQuery = true )
    void insertorderitem(@Param("order_id") long order_id,@Param("product_id") long product_id,@Param("quantity") int quantity, @Param("price") float price);
    @Query(value = "select * from order_item where order_id=?", nativeQuery = true )
    List<OrderItem> getByorder_id(long id);
    @Query(value = "select sum(quantity) from order_item where product_id=?", nativeQuery = true )
    int getBySumProduct(long id);
    @Query(value = "select * from order_item where product_id=?", nativeQuery = true )
    List<OrderItem> findByProduct_id(long id);
    @Query(value = "select * from order_item where order_id=?", nativeQuery = true )
    List<OrderItem> getByorder_id1(long id);
    @Query(value = "SELECT order_item.id,order_item.order_id,order_item.product_id,order_item.quantity,order_item.price FROM order_item INNER JOIN orderr ON order_item.order_id=orderr.id where orderr.status<?", nativeQuery = true )
    List<OrderItem> getBySmth(Long status);
}
